import unittest
from mycode import code


class SimpleTestCase(unittest.TestCase):

    def testA(self):
        n = 20
        k = 4
        
        total_list = code.generate_all_combinations(n, k)
        code.print_all_state(total_list)

if __name__ == "__main__":
    unittest.main() # run all tests
